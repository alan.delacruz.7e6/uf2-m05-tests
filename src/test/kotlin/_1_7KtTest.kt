import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_7KtTest{

    @Test
    fun negativenumber(){
        val expected = -1L
        assertEquals(expected, nextnumber(-2))
    }

    @Test
    fun bignumber(){
        val expected = 2582440533L
        assertEquals(expected, nextnumber(2582440532))
    }

    @Test
    fun bignegativenumber(){
        val expected = -4367854432L
        assertEquals(expected, nextnumber(-4367854433))
    }

}