import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import kotlin.math.exp

internal class _1_15KtTest{

    @Test
    fun bignumbers(){
        val expected = 21
        assertEquals(expected, addsecond(500))
    }

    @Test
    fun negativenumber(){
        val expected = -49
        assertEquals(expected, addsecond(-50))
    }

    @Test
    fun bignegativenumber(){
        val expected = -52
        assertEquals(expected, addsecond(-548633))
    }

}