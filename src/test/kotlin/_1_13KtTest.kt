import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_13KtTest{

    @Test
    fun negativenumbers(){
        val expected = -38.5
        assertEquals(expected, calcultemperature(-23.5, -15.0))
    }

    @Test
    fun onenegativenumber(){
        val expected = -21.900000000000002
        assertEquals(expected, calcultemperature(-45.6, 23.7))
    }

    @Test
    fun bignumbers(){
        val expected = 16397541.93
        assertEquals(expected, calcultemperature(15648887.45, 748654.48))
    }

}