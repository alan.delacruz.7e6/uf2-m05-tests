import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_4KtTest{

    @Test
    fun multiplynegativenumbers(){
        val expected = 225L
        assertEquals(expected, calcularea(-15, -15))
    }

    @Test
    fun multiplybignumbers(){
        val expected = 771864159364L
        assertEquals(expected, calcularea(878558, 878558))
    }

    @Test
    fun multiplyzero(){
        val expected = 0L
        assertEquals(expected, calcularea(0, 2))
    }

}