import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_11KtTest{

    @Test
    fun negativenumbers(){
        val expected = -13853.503999999999
        assertEquals(expected, calculair(-47.6, -21.4, -13.6))
    }

    @Test
    fun samenumber(){
        val expected = 64000.0
        assertEquals(expected, calculair(40.0, 40.0, 40.0))
    }

    @Test
    fun bignumber(){
        val expected = 55385414656.0
        assertEquals(expected, calculair(2342.0, 4352.0, 5434.0))
    }


}