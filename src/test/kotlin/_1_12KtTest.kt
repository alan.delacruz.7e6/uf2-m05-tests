import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_12KtTest{

    @Test
    fun numberzero(){
        val expected = 32.0
        assertEquals(expected, calculfahrenheit(0.0))
    }

    @Test
    fun negativenumber(){
        val expected = -10.30
        assertEquals(expected, calculfahrenheit(-23.5))
    }

    @Test
    fun bignumber(){
        val expected = 114.26
        assertEquals(expected, calculfahrenheit(45.7))
    }

}