import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_2KtTest{

    @Test
    fun multiplynegativenumber(){
        val expected = -1348L
        assertEquals(expected, duplicatenumber(-674))
    }

    @Test
    fun multiplybignumber(){
        val expected = 956302557144L
        assertEquals(expected, duplicatenumber(478151278572))
    }

    @Test
    fun multiplysmallnumber(){
        val expected = 70L
        assertEquals(expected, duplicatenumber(35))
    }



}