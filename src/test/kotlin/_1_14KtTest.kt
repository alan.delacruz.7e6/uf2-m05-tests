import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_14KtTest{

    @Test
    fun negativenumbers(){
        val expected = 0.34444444444444444
        assertEquals(expected, calccompte(-45, -15.50))
    }

    @Test
    fun bignumbers(){
        val expected = 2.919876522984379E-6
        assertEquals(expected, calccompte(54618748, 159.48))
    }

    @Test
    fun samenumber(){
        val expected = 1.0
        assertEquals(expected, calccompte(20, 20.0))
    }

}