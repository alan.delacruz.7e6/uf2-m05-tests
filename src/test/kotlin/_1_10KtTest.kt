import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_10KtTest{

    @Test
    fun negativenumber(){
        val expected = 9383568.520003136
        assertEquals(expected, calculmeasure(-3456.52))
    }

    @Test
    fun numberzero(){
        val expected = 0.1590431280879833
        assertEquals(expected, calculmeasure(0.45))
    }

    @Test
    fun bignumber(){
        val expected = 8.328658169535356E14
        assertEquals(expected, calculmeasure(32564362.324532))
    }

}