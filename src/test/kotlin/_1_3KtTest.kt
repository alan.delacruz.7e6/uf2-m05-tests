import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_3KtTest{

    @Test
    fun sumnegativenumber(){
        val expected = -570L
        assertEquals(expected, sumnumber(-450, -120))
    }

    @Test
    fun sumonenegative(){
        val expected = -160L
        assertEquals(expected, sumnumber(-210, 50))
    }

    @Test
    fun sumbignumber(){
        val expected = 217827414338
        assertEquals(expected, sumnumber(215672761554, 2154652784))
    }

}