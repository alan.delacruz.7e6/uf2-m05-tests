import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_8KtTest{

    @Test
    fun numberzero(){
        val expected = 0.0
        assertEquals(expected, doubledecimal(0.0))
    }

    @Test
    fun negativenumber(){
        val expected = -4.4
        assertEquals(expected, doubledecimal(-2.2))
    }

    @Test
    fun bignumbers(){
        val expected = -4.70647044E7
        assertEquals(expected, doubledecimal(-23532352.2))
    }

}