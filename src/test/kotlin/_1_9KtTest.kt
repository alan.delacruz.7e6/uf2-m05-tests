import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_9KtTest{

    @Test
    fun negativenumber(){
        val expected = 99.92572454319402
        assertEquals(expected, calculdiscount(-43637564.0, -32412.0))
    }

    @Test
    fun bignumbers(){
        val expected = 99.99044842233364
        assertEquals(expected, calculdiscount(432652347534.0, 41325125.0))
    }

    @Test
    fun negativepositive(){
        val expected = 105.57510135204046
        assertEquals(expected, calculdiscount(4363257.0, -243256.0))
    }


}