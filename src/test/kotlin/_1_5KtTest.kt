import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_5KtTest{

    @Test
    fun negativenumbers(){
        val expected = 112
        assertEquals(expected, calculboja(-4, -12, -7, -9))
    }

    @Test
    fun zeronumbers(){
        val expected = 0
        assertEquals(expected, calculboja(-53, 0, 0, 45))
    }

    @Test
    fun bignumbers(){
        val expected = 1989689512
        assertEquals(expected, calculboja(534745234, 5438792, 43583745, 234565))
    }

}