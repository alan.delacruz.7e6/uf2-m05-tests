import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_16KtTest{

    @Test
    fun negativenumber(){
        val expected = -25.0
        assertEquals(expected, transformint(-25))
    }

    @Test
    fun bignumber(){
        val expected = 43257345.0
        assertEquals(expected, transformint(43257345))
    }

    @Test
    fun numberzero(){
        val expected = 0.0
        assertEquals(expected, transformint(0))
    }

}