import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_6KtTest{

    @Test
    fun negativenumbers(){
        val expected = -46L
        assertEquals(expected, calculpupitres(-30, -22, -40))
    }
    @Test
    fun numberzero(){
        val expected = 0L
        assertEquals(expected, calculpupitres(0, 0, 0))
    }

    @Test
    fun bignumbers(){
        val expected = 176866258L
        assertEquals(expected, calculpupitres(4565426, 345634634, 3532456))
    }

}