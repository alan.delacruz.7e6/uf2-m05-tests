/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/21
* TITLE: 1.16 Transforma l'enter
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix enter: ")
    val enter = scanner.nextInt()
    print("Transformat decimal: ")
    print(transformint(enter))
}

fun transformint(enter: Int): Double{
    return (enter.toDouble())
}