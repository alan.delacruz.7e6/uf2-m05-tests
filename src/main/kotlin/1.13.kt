/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/21
* TITLE: 1.13 Quina temperatura fa?
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix la temperatura: ")
    val temperatura = scanner.nextDouble()
    println("Introdueix un augment: ")
    val augment = scanner.nextDouble()
    print("La temperatura actual és: ")
    print(calcultemperature(temperatura, augment))
}

fun calcultemperature(temperatura: Double, augment: Double): Double{
    return (temperatura + augment)
}