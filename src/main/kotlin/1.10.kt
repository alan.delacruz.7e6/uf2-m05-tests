/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/21
* TITLE: 1.10 Quina és la mida de la meva pizza?
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix diàmetre de la pizza: ")
    val userInputValue1 = scanner.nextDouble()
    print("Superfície de la pizza: ")
    print (calculmeasure(userInputValue1))
}

fun calculmeasure(userInputValue1: Double): Double{
    val radio = (userInputValue1 / 2 )
    val exponente = 2
    val cuadradoradio = (Math.pow(radio.toDouble(), exponente.toDouble()))
    return (Math.PI * cuadradoradio)
}

