/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/19
* TITLE: 1.3 Suma de dos nombres enters
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val userInputValue = scanner.nextInt().toLong()
    println("Introdueix un altre número: ")
    val userInputValue2 = scanner.nextInt().toLong()
    println(sumnumber(userInputValue, userInputValue2))
}

fun sumnumber(userInputValue: Long, userInputValue2: Long): Long {
    return (userInputValue + userInputValue2)
}
