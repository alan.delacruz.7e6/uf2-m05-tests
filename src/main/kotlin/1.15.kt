/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/21
* TITLE: 1.15 Afegeix un segon
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix els segons: ")
    val segons = scanner.nextInt()
    println("Número de segons: ")
    print(addsecond(segons))
}

fun addsecond(segons: Int): Int{
    return ((segons+1) % 60)
}