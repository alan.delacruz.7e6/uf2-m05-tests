/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/19
* TITLE: 1.2 Dobla l’enter
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val userInputValue = scanner.nextInt().toLong()
    println("Aquest és el número introduït: ")
    println(duplicatenumber(userInputValue))
}

fun duplicatenumber(userInputValue: Long): Long {
    return (userInputValue * 2)
}


