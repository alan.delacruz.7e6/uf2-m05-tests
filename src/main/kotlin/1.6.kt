/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/19
* TITLE: 1.6 Pupitres
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val userInputValue = scanner.nextInt().toLong()
    println("Introdueix un altre número: ")
    val userInputValue2 = scanner.nextInt().toLong()
    println("Introdueix un altre número: ")
    val userInputValue3 = scanner.nextInt().toLong()
    println(calculpupitres(userInputValue, userInputValue2, userInputValue3))
}

fun calculpupitres(userInputValue: Long, userInputValue2: Long, userInputValue3: Long): Long {
    val residuo = ((userInputValue + userInputValue2 + userInputValue3) % (2))
    return((userInputValue + userInputValue2 + userInputValue3) / (2) + (residuo))
}