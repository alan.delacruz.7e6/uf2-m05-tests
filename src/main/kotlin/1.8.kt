/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/21
* TITLE: 1.8 Dobla el decimal
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número decimal: ")
    val userInputValue = scanner.nextDouble()
    print("El número doblat és: ")
    print(doubledecimal(userInputValue))
}

fun doubledecimal(userInputValue: Double): Double{
    return (userInputValue * 2)
}