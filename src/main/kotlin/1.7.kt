/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/19
* TITLE: 1.7 Número següent
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val userInputValue = scanner.nextInt().toLong()
    print("El número següent és ")
    print(nextnumber(userInputValue))
}

fun nextnumber(userInputValue: Long): Long{
    return (userInputValue + 1)
}