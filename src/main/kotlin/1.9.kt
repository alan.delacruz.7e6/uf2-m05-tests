/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/21
* TITLE: 1.9 Calcula el descompte
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix preu actual: ")
    val userInputValue1 = scanner.nextDouble()
    println("Introdueix preu descompte: ")
    val userInputValue2 = scanner.nextDouble()
    print("El descompte és de: ")
    print (calculdiscount(userInputValue1, userInputValue2))
}

fun calculdiscount(userInputValue1: Double, userInputValue2: Double): Double{
    val resultado = (userInputValue1 - userInputValue2)
    return (resultado / userInputValue1 * 100)
}