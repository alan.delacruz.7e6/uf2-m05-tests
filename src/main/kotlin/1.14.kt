/*
* AUTHOR: Alan de la Cruz
* DATE: 2022/09/21
* TITLE: 1.14 Divisor de compte
*/
import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix número començals: ")
    val comencals = scanner.nextInt()
    println("Introdueix el cost del sopar: ")
    val costsopar = scanner.nextDouble()
    print("Cost per cada comencal: ")
    print(calccompte(comencals, costsopar))
}

fun calccompte(comencals: Int, costsopar: Double): Double{
    return (costsopar / comencals )
}